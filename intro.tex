\section{Basics}

\begin{frame}[t,fragile]{What is a contract?}
\begin{itemize}
  \item \textbf{Contract}: Associated to an operation.
    \begin{itemize}
      \item \textbf{Expectation} (precondition): Requirements to be satisfied prior to initiating an operation.
      \item \textbf{Commitment} (postcondition): Guarantees satisfied after finalizing an operation.
    \end{itemize}

  \item \textbf{Assertion}: May happen in any function body.
    \begin{itemize}
      \item A condition that needs to be satisfied in a given point in a program.
    \end{itemize}
\end{itemize}

\begin{columns}[T]

\column{.5\textwidth}

\begin{lstlisting}[basicstyle=\scriptsize]
void push(queue & q)
  [[expects: !q.full()]]
  [[ensures: !q.empty()]]
{
  // ...
  [[assert: q.is_ok()]]
  // ...
}
\end{lstlisting}

\column{.5\textwidth}

\begin{lstlisting}[basicstyle=\scriptsize]
void push(queue & q)
  [[pre: !q.full()]]
  [[post: !q.empty()]]
{
  // ...
  [[assert: q.is_ok()]]
  // ...
}
\end{lstlisting}

\end{columns}

\end{frame}

\begin{frame}[t]{Contracts in the interfaces}
\begin{itemize}
  \item A contract is part of an operation interface.
    \begin{itemize}
      \item They are not part of the type.
    \end{itemize}

  \vfill\pause
  \item \textbf{Contract specification}: Expressions in the contract.
    \begin{itemize}
      \item They are \emph{logically} part of the declaration of the operation.
    \end{itemize}

  \vfill\pause
  \item \textbf{Contract checking}: Code executing the checking.
    \begin{itemize}
      \item They are \emph{logically} part of the implementation of the operation.
      \item The observable behaviour is the same if the operation is invoked directly or indirectly.
    \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}[t,fragile]{Contracts in the body}
\begin{itemize}
  \item The body of an operation may contain contract assertions.
    \begin{itemize}
      \item An assertion may appear in any place of the operation body.
      \item It has access to implementation details.
      \item Effects of pre/post conditions may be expressed also as assertions.
      \item Assertions are not (usually) visible at the call site.
    \end{itemize}
\end{itemize}

\begin{columns}

\column{.5\textwidth}

\begin{lstlisting}
void f(char * p, int n)
  [[expects: p!=nullptr && n>0]]
  [[ensures: strlen(p)>0]]
{
  g(p,n);
  [[assert: p!=nullptr]]
  h(p,n);
}
\end{lstlisting}

\pause

\column{.5\textwidth}

\begin{lstlisting}
void f(char * p, int n)
{
  [[assert: p!=nullptr && n>0]]
  g(p,n);
  [[assert: p!=nullptr]]
  h(p,n);
  [[assert: strlen(p)>0]]
}
\end{lstlisting}


\end{columns}
\end{frame}

\section{Syntax details}

\begin{frame}[t]{Why attributes?}
\begin{itemize}
  \item Simple integration.
  \item No observable semantic effect for a correct program.
    \begin{itemize}
      \item \textbf{Note}: However implementations do not have the
            freedom to ignore contracts.
    \end{itemize}
  \item A correct program with contracts run with correct data will produce 
        the same allowed behavior as the same program without contract run 
        with the same data.
\end{itemize}
\end{frame}

\begin{frame}[t,fragile]{Declarations and definitions}
\begin{itemize}
  \item All declarations of the same operation must exactly specify the same contract.
  \item Contracts in definitions of operations must match exactly with contracts in declarations.
\end{itemize}

\begin{lstlisting}[basicstyle=\scriptsize]
void f(int x) [[expects: x>0]];
void f(int x); // Error missing contract
void f(int x) [[expects: x>0 && x<100]]; // Error contract mismatch

void g(char * p) [[expects: p!=nullptr]];
void g(char * p) { //... } // Error contract mismatch
\end{lstlisting}

\end{frame}

\begin{frame}[t,fragile]{Contracts and inheritance}
\begin{itemize}
%  \item Overridden functions inherit the contract from the base class.

  \item Overridden functions must have the same contract as the base class.
  \item If they repeat the contract it shall be exactly the same.
    \begin{itemize}
      \item For simplicity we do not allow weakening preconditions and
            strengthening postconditions.
      \item Although it should be the \emph{right} logical thing to do.
    \end{itemize}
\end{itemize}

\begin{columns}[t]

\column{.5\textwidth}
\begin{block}{Base class}
\begin{lstlisting}[basicstyle=\scriptsize]
class A {
public: 
  virtual void f() [[ensures: pred1()]];
  virtual void g() [[ensures: pred2()]];
};
\end{lstlisting}
%  virtual void h() [[ensures: pred3()]];
\end{block}

\column{.5\textwidth}
\begin{block}{Derived class}
\begin{lstlisting}[basicstyle=\scriptsize]
class B: public A {
public: 
  virtual void f() override 
    [[ensures: pred1() && q()]]; // Error
  virtual void g() override 
    [[ensures: pred2()]]; // OK
};
\end{lstlisting}
%  virtual void h() override; // Inherits
\end{block}

\end{columns}
\end{frame}

\begin{frame}[t,fragile]{What if I want to weaken a precondition?}
\begin{columns}[T]

\column{.5\textwidth}
\begin{block}{Ideal world}
\begin{lstlisting}[basicstyle=\scriptsize]
class A {
  // ...
  void f() [[expects: p1() && p2()]];
};

class B : public A {
  // ...
  void f() [[expects: p1()]];
};
\end{lstlisting}
\end{block}

\column{.5\textwidth}
\begin{block}{Real world}
\begin{lstlisting}[basicstyle=\scriptsize]
class A {
// ...
  void f() [[expects: p1()]]
  {
    [[assert: p2()]]
    //...
  }
};

class B : public A {
  // ...
  void f() [[expects: p1()]];
};
\end{lstlisting}
\end{block}

\end{columns}
\end{frame}

\begin{frame}[t,fragile]{What if I want to strengthen a postcondition?}
\begin{columns}[T]

\column{.5\textwidth}
\begin{block}{Ideal world}
\begin{lstlisting}[basicstyle=\scriptsize]
class A {
  // ...
  void f() [[ensures: p1()]];
};

class B : public A {
  // ...
  void f() [[ensures: p1() && p2()]];
};
\end{lstlisting}
\end{block}

\column{.5\textwidth}
\begin{block}{Real world}
\begin{lstlisting}[basicstyle=\scriptsize]
class A {
// ...
  void f() [[ensures: p1()]];
};

class B : public A {
  // ...
  void f() [[ensures: p1()]]
  {
    // ...
    [[assert: p2()]]
  }
};
\end{lstlisting}
\end{block}

\end{columns}
\end{frame}

\begin{frame}[t]{What can be accessed in a contract?}
\begin{itemize}
  \item A contract of a member function can only use members with the same or better
        visibility than that member function.
  \vspace{1em}
  \item Rules:
    \begin{itemize}
      \item Contract of public member functions can only reference public members.
      \item Contract of protected member functions can only reference public or protected members.
      \item Contract of private member function can reference all members.
    \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}[t,fragile]{Return values and old values}
\begin{itemize}
  \item Mechanism for using the returned value in postconditions.
  \item Syntax to be agreed.
\begin{lstlisting}
int f(int x) [[ ensures: return>0 ]]; // ?
int f(int x) [[ ensures: f ]]; // ?
int f(int x) [[ ensures: f(x) ]] // ?
\end{lstlisting}

  \vfill\pause
  \item Mechanism for accessing old values.
  \item Syntax to be agreed.
\begin{lstlisting}
int increment(int  & x) [[ensures: x == old(x) + 1]]; // ?
struct X {
  int x, y;
  int transform() [[ensures: x==old(y) && y==old(x)]]; // ?
};
\end{lstlisting}
\end{itemize}
\end{frame}

\begin{frame}[t]{What is left out?}
\begin{enumerate}
  \item Class invariants.
  \item Abstract states (contracts only on representation).
  \item Loop invariants.
\end{enumerate}
\end{frame}
